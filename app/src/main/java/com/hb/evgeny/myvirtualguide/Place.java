package com.hb.evgeny.myvirtualguide;

import com.google.android.gms.maps.model.LatLng;

import java.lang.reflect.Array;

/**
 * Created by Evgeny on 10.03.2017.
 */

public class Place {

    private String placeName;
    private double placeLat;
    private double placeLng;
    private float placeRating;
    private String placeType;
    private String address;

    //TODO Add places constructor or remove all this attributes to properties
    public Place(){

    }



    public void setAddress(String _address){
        this.address = _address;
    }
    public void setPlaceLat(double _placeLat){
        this.placeLat = _placeLat;
    }

    public void setPlaceLng(double _placeLng){
        this.placeLng = _placeLng;
    }

    public void setPlaceName(String _name){
        this.placeName = _name;
    }

    public void setPlaceRating(float _rating){
        this.placeRating = _rating;
    }

    public void setPlaceType(String _plType){
        this.placeType = _plType;
    }


    public String getPLaceName(){
        return this.placeName;
    }

    public LatLng getPlaceCoordinates(){
        LatLng coordinates = new LatLng(this.placeLat,this.placeLng);

        return coordinates;
    }

    public float getPlaceRating(){
        return this.placeRating;
    }

}
