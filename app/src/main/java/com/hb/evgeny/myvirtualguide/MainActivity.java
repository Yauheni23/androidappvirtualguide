package com.hb.evgeny.myvirtualguide;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

//import static com.hb.evgeny.myvirtualguide.R.id.map;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener{

    private Button btnZoomIn;
    private Button btnZoomOut;
    private Button gpsButton;
    private GoogleMap mapGoogle;
    private LatLng currentUserPosition;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String [] navDrawerStrings;

    private GoogleApiClient mGoogleApiClient;

    private LocationManager locMng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        btnZoomIn = (Button) findViewById(R.id.mapZoomIn);
        btnZoomIn.setOnClickListener(this);

        btnZoomOut = (Button) findViewById(R.id.mapZoomOut);
        btnZoomOut.setOnClickListener(this);

        gpsButton = (Button) findViewById(R.id.gpsTrackButton);
        gpsButton.setOnClickListener(this);

        locMng = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Navigation Drawer
        navDrawerStrings = new String[2];
        navDrawerStrings[0] = "Places";
        navDrawerStrings[1] = "About";

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.nav_drawer_item,navDrawerStrings));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
//        btnActTwo = (Button) findViewById(R.id.button);
//        btnActTwo.setOnClickListener(this);
//
//        mGoogleApiClient = new GoogleApiClient
//                .Builder(this)
//                .addApi(Places.GEO_DATA_API)
//                .addApi(Places.PLACE_DETECTION_API)
//                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
//                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
//                .build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // 53.900799, 30.330978
        mapGoogle = googleMap;


    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    protected void onStop() {
//        mGoogleApiClient.disconnect();
//        super.onStop();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //TODO change time to zero
        locMng.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                0, 10, locListener);
        locMng.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0,10,
                locListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locMng.removeUpdates(locListener);
    }

    //CODE FOR OPEN second activity
    //                Intent intent = new Intent(this, PlacesInfoActivity.class);
    //                startActivity(intent);

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mapZoomIn:
                mapGoogle.moveCamera(CameraUpdateFactory.zoomIn());
                break;
            case R.id.mapZoomOut:
                mapGoogle.moveCamera(CameraUpdateFactory.zoomOut());
                break;
            case R.id.gpsTrackButton:
            //TODO add gps tracking
                mapGoogle.moveCamera(CameraUpdateFactory.newLatLngZoom(currentUserPosition, 17));
                mapGoogle.addMarker(new MarkerOptions().position(currentUserPosition).title("Hi"));

                PlaceReceiving plReceive = new PlaceReceiving();
                plReceive.execute(makeURL().toString());


                //Circle circ = mapGoogle.addCircle(new CircleOptions().center(currentUserPosition).radius(500000).fillColor(Color.RED));
                break;
            default:
                break;
        }
    }
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
    private void selectItem(int position) {
        // update the main content by replacing fragments
        switch(position) {
            case 0: Intent intent = new Intent(this, PlacesInfoActivity.class);
                    startActivity(intent);
                break;
            case 1:
                Toast.makeText(this,"Not available",Toast.LENGTH_LONG).show();
                break;
        }
        mDrawerLayout.closeDrawer(mDrawerList);
        mDrawerList.setItemChecked(position, true);
    }

    private LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            currentUserPosition = new LatLng(location.getLatitude(),location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private StringBuilder makeURL(){
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location="+currentUserPosition.latitude+","+currentUserPosition.longitude);
        sb.append("&radius=5000");
        //TODO add types for choice
        sb.append("&types="+"food");
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyD7_ISNTatRm3PgbbOaIwNrnEMMQt5rnl4");

        return sb;
    }

    private String downloadUrl(String _strURL) throws IOException {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(_strURL);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class PlaceReceiving extends AsyncTask<String, Integer, String>{

        String data= null;


        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                e.printStackTrace();
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result){
            ParseJSON parserTask = new ParseJSON();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
    }

    private class ParseJSON extends AsyncTask<String, Integer, List<Place>>{

        JSONObject jsonObject;

        @Override
        protected List<Place> doInBackground(String... _jsonObject) {
            List<Place> placesList = null;
            PlacesJSONParse plPrsJson = new PlacesJSONParse();

            try{
                jsonObject = new JSONObject(_jsonObject[0]);

                /** Getting the parsed data as a List construct */
                placesList = plPrsJson.parseJson(jsonObject);

            }catch(Exception e){
                e.printStackTrace();
            }
            return placesList;

        }

        @Override
        protected void onPostExecute(List<Place> _plList){
            mapGoogle.clear();

            for (int i=0; i< _plList.size();i++){
                MarkerOptions markerOptions = new MarkerOptions();
                Place currPlace = new Place();
                currPlace = _plList.get(i);
                markerOptions.position(currPlace.getPlaceCoordinates());
                markerOptions.title(currPlace.getPLaceName());

                mapGoogle.addMarker(markerOptions);
            }
        }
    }


}
