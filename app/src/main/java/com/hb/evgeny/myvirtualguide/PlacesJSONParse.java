package com.hb.evgeny.myvirtualguide;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Evgeny on 05.04.2017.
 */

public class PlacesJSONParse {

    private static final String MY_API_WEB_KEY = "AIzaSyD7_ISNTatRm3PgbbOaIwNrnEMMQt5rnl4";

    private List<Place> placesList;
    private JSONArray jsonPLaces;
    //Example of HTTP request
    // https://maps.googleapis.com/maps/api/place/radarsearch/json?location=53.900762,30.330402&radius=50000&types=food&key=AIzaSyD7_ISNTatRm3PgbbOaIwNrnEMMQt5rnl4

    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=53.900799,30.330978&radius=5000&types=food&sensor=true&key=AIzaSyD7_ISNTatRm3PgbbOaIwNrnEMMQt5rnl4



    public List<Place> parseJson(JSONObject _jsonObject){
        jsonPLaces = null;

        try {
            jsonPLaces = _jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getPlaces(jsonPLaces);

    }

    private List<Place> getPlaces(JSONArray _jsonPLaces){
        int countPlaces = _jsonPLaces.length();

        placesList = new ArrayList<>();
        for (int i=0; i<countPlaces;i++){
            try {

                placesList.add(getPlaceInfo((JSONObject) _jsonPLaces.get(i)));
                //place = getPlace((JSONObject)jPlaces.get(i));
                //placesList.add(place);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return placesList;

    }

    private Place getPlaceInfo(JSONObject _jsonPLace){

        Place pl = new Place();

        try {
            if(!_jsonPLace.isNull("name")){
                pl.setPlaceName(_jsonPLace.getString("name"));
            }


            if(!_jsonPLace.isNull("vicinity")){
                pl.setAddress(_jsonPLace.getString("vicinity"));
            }

            pl.setPlaceLat(Double.parseDouble( _jsonPLace.getJSONObject("geometry").getJSONObject("location").getString("lat")));
            pl.setPlaceLng(Double.parseDouble( _jsonPLace.getJSONObject("geometry").getJSONObject("location").getString("lng")));

            if(!_jsonPLace.isNull("rating")){
                pl.setPlaceRating(Float.parseFloat(_jsonPLace.getString("rating")));
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pl;

    }
}
